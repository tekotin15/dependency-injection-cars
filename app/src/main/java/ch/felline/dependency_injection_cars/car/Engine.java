package ch.felline.dependency_injection_cars.car;

/**
 * Created by gfelline on 5/3/17.
 */

public interface Engine {

    String accelerate();

}
