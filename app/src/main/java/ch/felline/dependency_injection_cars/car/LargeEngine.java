package ch.felline.dependency_injection_cars.car;

/**
 * Created by gfelline on 5/4/17.
 */

public class LargeEngine implements Engine{
    @Override
    public String accelerate() {
        return "From 0 to 100 in 10 seconds!";
    }
}
