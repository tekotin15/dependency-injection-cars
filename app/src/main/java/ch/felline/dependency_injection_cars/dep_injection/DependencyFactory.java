package ch.felline.dependency_injection_cars.dep_injection;

import ch.felline.dependency_injection_cars.car.Car;
import ch.felline.dependency_injection_cars.car.Engine;
import ch.felline.dependency_injection_cars.car.Fiat;
import ch.felline.dependency_injection_cars.car.SmallEngine;

/**
 * Created by gfelline on 5/4/17.
 */

public class DependencyFactory{

    private static Car myCar;
    private static Engine myEngine;

    public static Engine providesMyEngine(){
        if(myEngine == null){
            myEngine = new SmallEngine();
        }
        return myEngine;
    }

    public static Car providesMyCar(){
        if(myCar == null){
            myCar = new Fiat(providesMyEngine());
        }
        return myCar;
    }
}