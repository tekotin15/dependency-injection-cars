package ch.felline.dependency_injection_cars.car;

import ch.felline.dependency_injection_cars.car.Car;
import ch.felline.dependency_injection_cars.car.Engine;
import ch.felline.dependency_injection_cars.car.Fiat;
import ch.felline.dependency_injection_cars.car.SmallEngine;
import dagger.Module;
import dagger.Provides;

/**
 * Created by gfelline on 5/4/17.
 */

@Module
public class CarModule {

    @Provides
    public Engine providesMyEngine() {
        return new LargeEngine();
    }

    @Provides
    public Car providesMyCar(Engine engine) {
        return new Fiat(engine);
    }

}
