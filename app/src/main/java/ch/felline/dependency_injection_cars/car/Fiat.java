package ch.felline.dependency_injection_cars.car;

/**
 * Created by gfelline on 5/3/17.
 */

public class Fiat implements Car {

    private Engine engine;

    public Fiat(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String move(int km) {

        return "Accelerating ....\n"+ engine.accelerate()  + "\nDriving " + km + " kilometers...";

    }
}
