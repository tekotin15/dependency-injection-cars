package ch.felline.dependency_injection_cars.root;

import javax.inject.Singleton;

import ch.felline.dependency_injection_cars.MainActivity;
import ch.felline.dependency_injection_cars.car.CarModule;

import dagger.Component;

/**
 * Created by gfelline on 4/27/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class, CarModule.class})
public interface ApplicationComponent {

    public void injectModules(MainActivity activity);
}
