package ch.felline.dependency_injection_cars.car;

/**
 * Created by gfelline on 5/3/17.
 */

public class SmallEngine implements Engine {

    @Override
    public String accelerate() {
        return "From 0 to 100 km/h in 1 minute";
    }
}
