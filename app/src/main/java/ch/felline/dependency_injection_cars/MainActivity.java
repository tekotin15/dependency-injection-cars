package ch.felline.dependency_injection_cars;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import ch.felline.dependency_injection_cars.car.Car;
import ch.felline.dependency_injection_cars.root.App;

public class MainActivity extends AppCompatActivity {

    @Inject
    Car car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getComponent().injectModules(this);
    }


    public void move(View v) {
        Toast.makeText(this, car.move(1), Toast.LENGTH_SHORT).show();
    }
}
