package ch.felline.dependency_injection_cars.root;

import android.app.Application;

import ch.felline.dependency_injection_cars.car.CarModule;

/**
 * Created by gfelline on 4/27/17.
 */

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .carModule(new CarModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
